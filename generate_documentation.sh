#!/usr/bin/env bash

########################
#
# Function : 
#	Generate documentation as rst files, and then make html files from rst content. 
########################

python doc/generate_sphinx_rst.py -n ts_cluster -d doc/sphinx/ -s rst -f .
cd doc/sphinx 
make html
cd ../../
mv doc/sphinx/_build/html doc/html
