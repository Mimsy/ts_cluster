/!\ J'écris ici des idées en vrac, ça n'a rien à faire dans le README, on le changera après
-------------------------------------------------------------------------------------------

This Python module provides various functions to provide various clustering methods (and clustering indicators) for time series.

Various notebooks will be included, to show use cases of each models.

This package is and will remain open-source.


Idées de notions à implémenter:
-----------

- Un objet qui représente une série temporelle ? Avec différents indicateurs ? 
- Un objet par type de clustering ? Par type de distance ?
- Un objet qui permet une comparaison rapide des résultats de différentes méthodes
- Un objet qui permet d'interpréter facilement les résultats
- Un objet qui permet de visualiser facilement les résultats

Lien de référence : https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=0ahUKEwjV96Tn69DbAhVGbRQKHe6VDYwQFghKMAI&url=https%3A%2F%2Fwww.jstatsoft.org%2Farticle%2Fview%2Fv062i01%2Fv62i01.pdf&usg=AOvVaw1U5c5lkvUE6h88ZsiH-N-d