"""
Time serie object.
"""
import pandas as pd
import numpy as np

class TimeSerie:
    """
    TimeSerie object, which takes as an initialization argument a :
        - List
        - Numpy array
        - Pandas  serie
    """

    def __init__(self, values):
        """
        :param values:
        :type values: List, numpy array or pandas serie
        """

        if not any([isinstance(values, np.array), isinstance(values, pd.Series), isinstance(values, list)]):
            print("Values should either be a list, a numpy array or a pandas serie")
            raise TypeError

        self.serie = pd.Series(values)
        self.mean = self.serie.mean()
        self.sd = self.serie.sd()
        self.acf = self.serie.autocorr()
