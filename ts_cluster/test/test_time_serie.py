"""
Tests the TimeSerie class
"""
from ts_cluster import time_serie as ts

import unittest

import numpy as np
import pandas as pd

class TestTimeSerie(unittest.TestCase):

    def test_NumpyArray(self):
        """
        Tests that the TimeSerie object is properly created when given a numpy array
        """
        test_values = np.array([1,2,3,2,4])
        try:
            ts.TimeSerie(test_values)
        except:
            print("Could not create time serie object using a numpy array")

    def test_List(self):
        """
        Tests that the TimeSerie object is properly created when given a list
        """
        test_list = [1,2,3,2,4]
        try:
            ts.TimeSerie(test_list)
        except:
            print("Could not create time serie from list")

    def test_PandasSerie(self):
        """
        Tests that the TimeSerie object is properly created when given a pandas serie
        """
        test_serie = pd.Series([1,2,3,2,4])
        try:
            ts.TimeSerie(test_serie)
        except:
            print("Could not create time serie from panda series")

    def test_FailOnDifferentType(self):
        """
        Tests that the TimeSerie object does not build when given an object which is not a numpy array or a pandas serie or a list
        """
        test_tuple = (1,2,3,3,4)
        with self.assertRaises(TypeError):
            ts.TimeSerie(test_tuple)


if __name__=="__main__":
    unittest.main()
