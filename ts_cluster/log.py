# -*- python -*-

import logging
from logging import handlers
import sys


class MaxLevelFilter(logging.Filter):
    """
    Class used to filter lower than specified level. It is set as a filter for the stdout handler, because we don't     want warning to go to stdout but to stderr. Users should never have to use this class.
    From http://stackoverflow.com/a/24956305/1076493
    """
    def __init__(self, level):
        self.level = level

    def filter(self, record):
        return record.levelno < self.level


class StreamHandler(logging.StreamHandler):
    """
    Inherited from logging.StreamHandler class.
    """
    def __init__(self, *args, **kwargs):
        super(StreamHandler, self).__init__(*args, **kwargs)


class RotatingFileHandler(handlers.RotatingFileHandler):
    """
    Inherited from logging.handlers.StreamHandler class.
    """
    def __init__(self, *args, **kwargs):
        super(RotatingFileHandler, self).__init__(*args, **kwargs)


class Formatter(logging.Formatter):
    """
    Inherited from logging.Formatter class.
    """
    def __init__(self, format='%(asctime)s %(levelname)s [%(filename)s]: %(message)s'):
        super(Formatter, self).__init__(fmt=format)


def create_logger(filepath=None,
                  logger="root",
                  mode="a",
                  maxBytes=1000000,
                  backupCount=5,
                  logger_level="DEBUG",
                  file_level="INFO",
                  stdout_level="DEBUG",
                  stderr_level="WARNING"):
    """
    Returns a new logger object with attributes set based on the given arguments:

    :param filepath: Path to the file where logs should be written. Default None, logs are written to stdout, stderr.
    :type filepath: str

    :param logger: The name of the logger object. Default to "root"
    :type logger: str

    :param mode: Mode when writing to file. Used only when filepath is not None.
    :type mode: str

    :param maxBytes: Maximum number of bytes written to file before rotating. Default to 1.000.000
    :type maxBytes: int

    :param backupCount: Maximum number of files to keep when rotating. Default to 5.
    :type backupCount: int

    :param logger_level: Level of main logger. Default to "DEBUG"
    :type logger_level: str

    :param file_level: Level of rotating file handler. Default to "INFO"
    :type file_level: str

    :param stdout_level: Level of stdout stream handler. Default to "DEBUG"
    :type stdout_level: str

    :param stderr_level: Level of stderr stream handler. Default to "WARNING"
    :type stderr_level: str

    :return: A logger of class logging.logger
    :rtype: logging.logger
    """

    logger = logging.getLogger(logger)
    if logger.hasHandlers():
        logger.handlers.clear()

    formatter = Formatter()

    logging_stdout = StreamHandler(sys.stdout)
    logging_stderr = StreamHandler(sys.stderr)

    logging_stdout.setFormatter(formatter)
    logging_stderr.setFormatter(formatter)

    logging_stdout.addFilter(MaxLevelFilter(logging.WARNING))

    logging_stdout.setLevel(stdout_level)
    logging_stderr.setLevel(stderr_level)

    if filepath is not None:
        file_handler = RotatingFileHandler(filepath, mode=mode, maxBytes=maxBytes, backupCount=backupCount)
        file_handler.setLevel(file_level)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    logger.addHandler(logging_stdout)
    logger.addHandler(logging_stderr)

    logger.setLevel(logger_level)
    return logger

