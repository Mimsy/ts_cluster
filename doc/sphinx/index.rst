.. You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*****
Welcome to ts_cluster documentation!
*****

This page must be written in reStructured Text (RST) 
########

Not only you can write bullet lists, but also :
*****************


* in *in italic* 


* in **bold**


* links : 	`python <www.python.org>`_ 	


* ``in verbatim`` 	


======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
