Created this file in order to discuss the general ideas in this package:

# Classes to create

## The TS class : represents a time serie

### Attributes
Global structure = Numpy array or pandas serie ? From which should we inherit ?

- Stat indicators
    - Mean
    - SD
    - ACF


### Methods

- Plot methods

## The dissimilarity class

# SKLEARN dependencies 

Should we depend on sklearn (for the clustering algorithms for example) or code our own versions ?

# Demos dataset

Should we include demos dataset ? And use Jupyter notebooks to show how 
