# -*- python -*-

import setuptools
from setuptools import setup

                
def readme():
    with open('README.md') as f:
        return f.read()

def requirements():
    with open('requirements.txt') as f:
        return [line.strip() for line in f]


setup(name='ts_cluster',
    version='0.1',
    description='',
    long_description=readme(),
    classifiers=[
    ],
    keywords='',
    url='',
    author='',
    author_email='',
    license='MIT',
    packages=['ts_cluster'],
    test_suite = 'nose.collector',
    tests_require = ['nose'],
    zip_safe=False,
    install_requires=requirements(),
    scripts=[] #Put the scripts placed in /bin folder here so that they can be used directly from command line.
    )
